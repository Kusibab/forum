package com.Kusibab.forum.forum.controller;

import com.Kusibab.forum.forum.model.entity.Comment;
import com.Kusibab.forum.forum.model.repository.RatingRepository;
import com.Kusibab.forum.forum.model.repository.UserRepository;
import com.Kusibab.forum.forum.model.service.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@EnableAutoConfiguration
public class RatingController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RatingRepository ratingRepository;

    @Autowired
    RatingService ratingService;


    @PostMapping("/incrementPoints")
    public String incrementPoints(@RequestParam(value = "id") Long id,
                                  @RequestParam(value = "username") String username, Model model) {
        ratingService.updateRatingPoints(id, username, 1, model);

        model.addAttribute("comment", new Comment());
        model.addAttribute("allUsers", userRepository);
        model.addAttribute("allRatings", ratingRepository);
        return "topicView";
    }

    @PostMapping("/decrementPoints")
    public String decrementPoints(@RequestParam(value = "id") Long id,
                                  @RequestParam(value = "username") String username, Model model) {
        ratingService.updateRatingPoints(id, username, -1, model);
        model.addAttribute("comment", new Comment());
        model.addAttribute("allUsers", userRepository);
        model.addAttribute("allRatings", ratingRepository);
        return "topicView";
    }
}
