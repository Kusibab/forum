package com.Kusibab.forum.forum.controller;

import com.Kusibab.forum.forum.model.entity.User;
import com.Kusibab.forum.forum.model.mailSender.EmailSender;
import com.Kusibab.forum.forum.model.repository.UserRepository;
import com.Kusibab.forum.forum.model.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@EnableAutoConfiguration
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserService userService;

    @Autowired
    private EmailSender emailSender;

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/notLogged/registrationGet")
    public String getRegistration(Model model) {
        User user = new User();
        model.addAttribute("user", user);
        return "registration";
    }

    @GetMapping("/notLogged/passwordResetShow")
    public String passwordResetShow() {
        return "passwordReset";
    }

    @PostMapping("/notLogged/registrationPost")
    public String registration(@ModelAttribute("user") @Valid User user,
                               BindingResult bindingResult,
                               @RequestParam(value = "passwordConfirm") String passwordConfirm,
                               HttpServletRequest request) {
        if (!user.getPassword().equals(passwordConfirm)) {
            bindingResult.rejectValue("password", "Diff.user.passwordConfirm", "Passwords are different");
            return "registration";
        }

        if (bindingResult.hasErrors()) {
            return "registration";
        }

        if (userRepository.findByEmail(user.getEmail()) != null) {
            bindingResult.rejectValue("email", "Duplicate.user.email", "Email is already in use");
            return "registration";
        }

        if (userRepository.findByUsername(user.getUsername()) != null) {
            bindingResult.rejectValue("username", "Duplicate.user.username", "Username has been taken");
            return "registration";
        }
        userService.addUser(user, request);
        return "login";
    }

    @GetMapping("/seeUser")
    public String seeUser(@RequestParam Long userId, Model model) {
        model.addAttribute("actualUser", userRepository.findById(userId));
        model.addAttribute("allUsers", userRepository);
        return "userView";
    }

    @PostMapping("/userSettings")
    public String settings(@RequestParam String user, Model model) {
        model.addAttribute("user", userRepository.findByUsername(user));
        return "userSettingsView";
    }

    @PostMapping("/changePassword")
    public String changePassword(@ModelAttribute("user") @Valid User user,
                                 BindingResult bindingResult,
                                 @RequestParam("oldPassword") String oldPassword,
                                 @RequestParam("oldPasswordInput") String oldPasswordInput,
                                 @RequestParam("passwordConfirm") String passwordConfirm,
                                 Model model) {
        if (!oldPassword.equals(oldPasswordInput)) {
            bindingResult.rejectValue("password", "Diff.user.passwordConfirm", "Password is incorrect!");
            return "userSettingsView";
        }
        if (!user.getPassword().equals(passwordConfirm)) {
            bindingResult.rejectValue("password", "Diff.user.passwordConfirm", "Passwords are different!");
            return "userSettingsView";
        }
        if (bindingResult.hasErrors()) {
            return "userSettingsView";
        }
        model.addAttribute("user", userService.updatePassword(user));
        bindingResult.rejectValue("password", "xx", "Password has benn changed");
        return "mainView";
    }

    @PostMapping("/deleteUser")
    public String deleteUser(@ModelAttribute("user") @Valid User user,
                             BindingResult bindingResult,
                             @RequestParam("passwordConfirm") String passwordConfirm) {
        if (!user.getPassword().equals(passwordConfirm)) {
            bindingResult.rejectValue("password", "Diff.user.passwordConfirm", "Passwords are different!");
            return "userSettingsView";
        }
        if (bindingResult.hasErrors()) {
            return "userSettingsView";
        }
        userService.deleteUser(user);
        return "index";
    }

    @PostMapping("/deleteUserByModerator")
    public String deleteUserByModerator(@ModelAttribute("user") @Valid User user,
                                        BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "userView";
        }
        userService.deleteUser(user);
        return "index";
    }

    @GetMapping("/notLogged/confirm")
    public String showConfirmationPage(@RequestParam("token") String token, Model model) {
        User user = userRepository.findByConfirmationToken(token);
        if (user == null) {
            model.addAttribute("confirmationToken", false);
        } else {
            user.setEnabled(true);
            userRepository.save(user);
            model.addAttribute("confirmationToken", true);
        }
        return "confirmation";
    }


}
