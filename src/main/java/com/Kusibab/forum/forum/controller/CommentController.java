package com.Kusibab.forum.forum.controller;

import com.Kusibab.forum.forum.model.entity.Comment;
import com.Kusibab.forum.forum.model.repository.CommentRepository;
import com.Kusibab.forum.forum.model.repository.RatingRepository;
import com.Kusibab.forum.forum.model.repository.TopicRepository;
import com.Kusibab.forum.forum.model.repository.UserRepository;
import com.Kusibab.forum.forum.model.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
@EnableAutoConfiguration
public class CommentController {

    @Autowired
    TopicRepository topicRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    CommentRepository commentRepository;

    @Autowired
    RatingRepository ratingRepository;

    @Autowired
    CommentService commentService;


    @PostMapping("/addComment")
    public String addComment(@ModelAttribute("comment") @Valid Comment comment,
                             BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "topicView";
        }
        commentService.addComment(comment, model);
        model.addAttribute("allUsers", userRepository);
        model.addAttribute("allRatings", ratingRepository);
        return "topicView";
    }


    @PostMapping("/editComment")
    public String editComment(@RequestParam(value = "id") Long id, Model model) {
        Comment comment = commentRepository.findById(id);
        model.addAttribute("actualComment", comment);
        model.addAttribute("actualTopic", comment.getTopic());
        return "editingComment";
    }

    @PostMapping("/setNewComment")
    public String setNewComment(@ModelAttribute("comment") @Valid Comment comment,
                                BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "editingComment";
        }
        commentService.editComment(comment, model);
        model.addAttribute("allUsers", userRepository);
        model.addAttribute("allRatings", ratingRepository);
        return "topicView";
    }
}
