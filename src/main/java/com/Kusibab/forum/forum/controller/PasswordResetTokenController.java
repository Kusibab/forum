package com.Kusibab.forum.forum.controller;

import com.Kusibab.forum.forum.model.entity.PasswordResetToken;
import com.Kusibab.forum.forum.model.entity.User;
import com.Kusibab.forum.forum.model.repository.PasswordResetTokenRepository;
import com.Kusibab.forum.forum.model.repository.UserRepository;
import com.Kusibab.forum.forum.model.service.PasswordResetTokenService;
import com.Kusibab.forum.forum.model.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@EnableAutoConfiguration
public class PasswordResetTokenController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordResetTokenService passwordResetTokenService;

    @Autowired
    PasswordResetTokenRepository passwordResetTokenRepository;

    @Autowired
    UserService userService;

    @GetMapping("/notLogged/passwordChange")
    public String reset(HttpServletRequest request, @RequestParam("token") String token, Model model) {
        PasswordResetToken passwordResetToken = passwordResetTokenRepository.findByPasswordToken(token);
        if (passwordResetToken == null) {
            model.addAttribute("resetErr", "tokenNotFound");
            return "updatePassword";
        }
        if (passwordResetToken.getExpiryDate().getTime() - System.currentTimeMillis() <= 0) {
            model.addAttribute("resetErr", "tokenHasExpired");
            return "updatePassword";
        }
        if (passwordResetToken.getUser() == null) {
            model.addAttribute("resetErr", "userDoNotExist");
            return "updatePassword";
        }
        if (!passwordResetToken.getUser().isEnabled()) {
            model.addAttribute("resetErr", "userNotConfirmed");
            return "updatePassword";
        }
        model.addAttribute("resetErr", "NoErrors");
        model.addAttribute("user", passwordResetToken.getUser());
        return "updatePassword";
    }

    @PostMapping("/notLogged/passwordRst")
    public String passwordRst(@ModelAttribute("user") @Valid User user,
                              BindingResult bindingResult,
                              @RequestParam("emailConfirm") String emailConfirm,
                              @RequestParam("passwordConfirm") String passwordConfirm,
                              Model model) {
        User userEdit = userRepository.findByUsername(user.getUsername());
        model.addAttribute("resetErr", "NoErrors");
        if (!user.getPassword().equals(passwordConfirm)) {
            bindingResult.rejectValue("password", "Diff.user.passwordConfirm", "Passwords are different!");
            return "updatePassword";
        }
        if (!userEdit.getEmail().equals(emailConfirm)) {
            bindingResult.rejectValue("email", "xx", "Email is not correct");
        }
        if (bindingResult.hasErrors()) {
            return "updatePassword";
        }
        model.addAttribute("user", userService.updatePassword(user));
        bindingResult.rejectValue("password", "xx", "Password has benn changed");
        return "updatePassword";
    }

    @GetMapping("/notLogged/resetPassword")
    public String resetPassword(HttpServletRequest request, @RequestParam("email") String email, Model model) {

        if (passwordResetTokenService.resetPassword(request, email).equals("OK")) {
            model.addAttribute("emailSent", true);
        } else {
            model.addAttribute("emailSent", false);
        }
        return "passwordReset";
    }


}
