package com.Kusibab.forum.forum.controller;

import com.Kusibab.forum.forum.model.entity.Comment;
import com.Kusibab.forum.forum.model.entity.Topic;
import com.Kusibab.forum.forum.model.repository.CommentRepository;
import com.Kusibab.forum.forum.model.repository.RatingRepository;
import com.Kusibab.forum.forum.model.repository.TopicRepository;
import com.Kusibab.forum.forum.model.repository.UserRepository;
import com.Kusibab.forum.forum.model.service.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
@EnableAutoConfiguration
public class TopicController {

    @Autowired
    TopicRepository topicRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    CommentRepository commentRepository;

    @Autowired
    RatingRepository ratingRepository;

    @Autowired
    TopicService topicService;


    @PostMapping("/addNewTopic")
    public String addNewTopic(@ModelAttribute("topic") @Valid Topic topic,
                              BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "mainView";
        }
        topicService.addTopic(topic);
        model.addAttribute("allTopics", topicRepository.findAll());
        return "mainView";
    }

    @GetMapping("/allTopics")
    public String allTopics(Model model) {
        model.addAttribute("topic", new Topic());
        model.addAttribute("allTopics", topicRepository.findAll());
        return "mainView";
    }

    @GetMapping("/seeTopic")
    public String seeTopic(@RequestParam Long topicId, Model model) {
        model.addAttribute("actualTopic", topicRepository.findById(topicId));
        model.addAttribute("comment", new Comment());
        model.addAttribute("actualComments", topicRepository.findById(topicId).getCommentList());
        model.addAttribute("allUsers", userRepository);
        model.addAttribute("allRatings", ratingRepository);
        return "topicView";
    }

    @PostMapping("/deleteTopic")
    public String deleteTopic(@ModelAttribute("topic") @Valid Topic topic,
                              BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("allTopics", topicRepository.findAll());
            return "mainView";
        }
        topicService.deleteTopic(topic);
        model.addAttribute("allTopics", topicRepository.findAll());
        model.addAttribute("allUsers", userRepository);
        return "mainView";
    }


}
