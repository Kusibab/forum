package com.Kusibab.forum.forum.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class MvcConfiguration extends WebMvcConfigurerAdapter {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("index");
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/registration").setViewName("registration");
        registry.addViewController("/mainView").setViewName("mainView");
        registry.addViewController("/topicView").setViewName("topicView");
        registry.addViewController("/userView").setViewName("userView");
        registry.addViewController("/userSettingsView").setViewName("userSettingsView");
        registry.addViewController("/passwordReset").setViewName("passwordReset");
        registry.addViewController("/confirmation").setViewName("confirmation");
        registry.addViewController("/updatePassword").setViewName("updatePassword");
    }
}
