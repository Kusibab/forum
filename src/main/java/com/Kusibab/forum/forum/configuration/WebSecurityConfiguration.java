package com.Kusibab.forum.forum.configuration;

import com.Kusibab.forum.forum.model.entity.User;
import com.Kusibab.forum.forum.model.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserRepository userRepository;

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.csrf().disable();
        httpSecurity.
                authorizeRequests()
                .antMatchers("/", "/login", "/notLogged/**", "/css/**").permitAll()
                .anyRequest().authenticated()
                .and().formLogin().loginPage("/login").permitAll()
                .and().logout().permitAll();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder builder) throws Exception {

        for (User user : userRepository.findAll()) {
            builder.inMemoryAuthentication()
                    .withUser(user.getUsername()).password(user.getPassword()).roles(user.getRole().toString()).disabled(!user.isEnabled());

        }
    }
}
