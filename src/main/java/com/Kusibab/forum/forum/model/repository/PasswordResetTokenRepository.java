package com.Kusibab.forum.forum.model.repository;

import com.Kusibab.forum.forum.model.entity.PasswordResetToken;
import com.Kusibab.forum.forum.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PasswordResetTokenRepository extends JpaRepository<PasswordResetToken, Long> {
    PasswordResetToken findByPasswordToken(String passwordToken);

    PasswordResetToken findByUser(User user);
}
