package com.Kusibab.forum.forum.model.repository;

import com.Kusibab.forum.forum.model.entity.PasswordResetToken;
import com.Kusibab.forum.forum.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findById(Long id);

    User findByEmail(String email);

    User findByPasswordResetToken(PasswordResetToken passwordResetToken);

    User findByConfirmationToken(String confirmationToken);

    User findByUsername(String username);

}
