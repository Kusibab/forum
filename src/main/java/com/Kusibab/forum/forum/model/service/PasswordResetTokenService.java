package com.Kusibab.forum.forum.model.service;


import com.Kusibab.forum.forum.model.entity.PasswordResetToken;
import com.Kusibab.forum.forum.model.entity.User;
import com.Kusibab.forum.forum.model.mailSender.EmailSender;
import com.Kusibab.forum.forum.model.repository.PasswordResetTokenRepository;
import com.Kusibab.forum.forum.model.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.UUID;

@Service
public class PasswordResetTokenService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordResetTokenRepository passwordResetTokenRepository;

    @Autowired
    EmailSender emailSender;

    public String resetPassword(HttpServletRequest request, String email) {
        String message = createPasswordToken(email);
        if (!message.equals("OK")) {
            return message;
        }
        User user = userRepository.findByEmail(email);

        String appUrl = request.getScheme() + "://" + request.getServerName();

        SimpleMailMessage registrationEmail = new SimpleMailMessage();
        registrationEmail.setTo(user.getEmail());
        registrationEmail.setSubject("Password change");
        registrationEmail.setText("To reset your password, please click the link below: \n" +
                appUrl + ":8180/passwordChange?token=" + user.getPasswordResetToken().getPasswordToken());
        registrationEmail.setFrom("spring.mail.username");

        emailSender.sendEmail(user.getEmail(), registrationEmail.getSubject(), registrationEmail.getText());
        return message;
    }

    private String createPasswordToken(String email) {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            return "User do not exist";
        }
        PasswordResetToken resetToken = user.getPasswordResetToken();

        if (resetToken == null) {
            resetToken = new PasswordResetToken();
            resetToken.setPasswordToken(UUID.randomUUID().toString());
        }
        if (resetToken.getExpiryDate().getTime() - System.currentTimeMillis() <= 0) {
            resetToken.setPasswordToken(UUID.randomUUID().toString());
        }
        resetToken.setUser(user);
        resetToken.setExpiryDate(new Timestamp(System.currentTimeMillis() + 24 * 60 * 60 * 1000));
        user.setPasswordResetToken(resetToken);
        passwordResetTokenRepository.save(resetToken);
        userRepository.save(user);

        return "OK";
    }


}
