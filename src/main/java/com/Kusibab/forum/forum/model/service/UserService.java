package com.Kusibab.forum.forum.model.service;

import com.Kusibab.forum.forum.model.entity.Comment;
import com.Kusibab.forum.forum.model.entity.Topic;
import com.Kusibab.forum.forum.model.entity.User;
import com.Kusibab.forum.forum.model.mailSender.EmailSender;
import com.Kusibab.forum.forum.model.repository.CommentRepository;
import com.Kusibab.forum.forum.model.repository.PasswordResetTokenRepository;
import com.Kusibab.forum.forum.model.repository.TopicRepository;
import com.Kusibab.forum.forum.model.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

@Service
public class UserService {

    @Autowired
    EmailSender emailSender;

    @Autowired
    UserRepository userRepository;

    @Autowired
    TopicRepository topicRepository;

    @Autowired
    CommentRepository commentRepository;

    @Autowired
    PasswordResetTokenRepository passwordResetTokenRepository;


    public void addUser(User user, HttpServletRequest request) {
        user.setConfirmationToken(UUID.randomUUID().toString());
        user.setRole(User.UserRole.USER);
        userRepository.save(user);

        String appUrl = request.getScheme() + "://" + request.getServerName();

        SimpleMailMessage registrationEmail = new SimpleMailMessage();
        registrationEmail.setTo(user.getEmail());
        registrationEmail.setSubject("Registration Confirmation");
        registrationEmail.setText("To confirm your e-mail address, please click the link below: \n" +
                appUrl + ":8180/confirm?token=" + user.getConfirmationToken());
        registrationEmail.setFrom("spring.mail.username");

        emailSender.sendEmail(user.getEmail(), registrationEmail.getSubject(), registrationEmail.getText());
    }


    public User updatePassword(User newUserData) {
        User editedUser = userRepository.findByUsername(newUserData.getUsername());
        emailSender.sendEmail(editedUser.getEmail(), "Password has been changed.", "Hello: " + newUserData.getUsername() + "!" + "\n Your password has been changed!");
        editedUser.setPassword(newUserData.getPassword());
        userRepository.save(editedUser);
        editedUser.setPasswordResetToken(null);
        passwordResetTokenRepository.delete(passwordResetTokenRepository.findByUser(editedUser));
        return editedUser;
    }

    public void deleteUser(User user) {
        User delUser = userRepository.findByUsername(user.getUsername());
        delUser.getTopicList().clear();
        for (Topic topic : topicRepository.findAllByUser(delUser)) {
            topic.setUser(null);
        }
        delUser.getCommentList().clear();
        for (Comment comment : commentRepository.findAllByUser(delUser)) {
            comment.setUser(null);
        }
        delUser.getPasswordResetToken().setUser(null);
        userRepository.delete(delUser);
    }


}
