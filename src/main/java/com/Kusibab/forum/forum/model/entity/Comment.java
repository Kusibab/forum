package com.Kusibab.forum.forum.model.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.sql.Timestamp;

@Entity
@Table(name = "comment")
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;


    @Column(name = "creation_date")
    @DateTimeFormat
    private Timestamp date;

    @Column(name = "comment_points")
    private Integer points;

    @ManyToOne
    @JoinColumn(name = "TOPIC_ID")
    private Topic topic;

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private User user;

    @Column(name = "comment_text")
    @Size(min = 3, message = "must contain 3 characters!")
    private String commentText;

    /*@OneToMany(fetch = FetchType.LAZY, mappedBy = "comment")
    private List<Rating> ratings;*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }


    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

  /*  public List<Rating> getRatings() {
        return ratings;
    }

    public void setRatings(List<Rating> ratings) {
        this.ratings = ratings;
    }*/
}
