package com.Kusibab.forum.forum.model.service;

import com.Kusibab.forum.forum.model.entity.Comment;
import com.Kusibab.forum.forum.model.entity.Rating;
import com.Kusibab.forum.forum.model.repository.CommentRepository;
import com.Kusibab.forum.forum.model.repository.RatingRepository;
import com.Kusibab.forum.forum.model.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

@Service
public class RatingService {

    @Autowired
    RatingRepository ratingRepository;

    @Autowired
    CommentRepository commentRepository;

    @Autowired
    UserRepository userRepository;

    public void updateRatingPoints(Long id, String username, int ratingValue, Model model) {
        Rating rating = new Rating();
        Comment comment = commentRepository.findById(id);
        comment.setPoints(comment.getPoints() + ratingValue);
        commentRepository.save(comment);
        rating.setComment(comment);
        rating.setUser(userRepository.findByUsername(username));
        ratingRepository.save(rating);
        model.addAttribute("actualComments", comment.getTopic().getCommentList());
        model.addAttribute("actualTopic", comment.getTopic());
    }
}
