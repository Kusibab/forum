package com.Kusibab.forum.forum.model.mailSender;

public interface EmailSender {
    void sendEmail(String to, String subject, String content);
}
