package com.Kusibab.forum.forum.model.service;

import com.Kusibab.forum.forum.model.entity.Comment;
import com.Kusibab.forum.forum.model.entity.Topic;
import com.Kusibab.forum.forum.model.entity.User;
import com.Kusibab.forum.forum.model.repository.CommentRepository;
import com.Kusibab.forum.forum.model.repository.TopicRepository;
import com.Kusibab.forum.forum.model.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
public class TopicService {

    @Autowired
    TopicRepository topicRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    CommentRepository commentRepository;

    public void addTopic(Topic topic) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        User user = userRepository.findByUsername(username);
        topic.setUser(user);
        topic.setDate(new Timestamp(System.currentTimeMillis()));
        topicRepository.save(topic);
        user.getTopicList().add(topic);
    }

    public void deleteTopic(Topic topic) {
        Topic delTopic = topicRepository.findById(topic.getId());
        delTopic.getCommentList().clear();
        for (Comment comment : commentRepository.findAllByTopic(delTopic)) {
            comment.setTopic(null);
        }
        topicRepository.delete(delTopic);
    }
}
