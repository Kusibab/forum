package com.Kusibab.forum.forum.model.service;

import com.Kusibab.forum.forum.model.entity.Comment;
import com.Kusibab.forum.forum.model.entity.Topic;
import com.Kusibab.forum.forum.model.entity.User;
import com.Kusibab.forum.forum.model.repository.CommentRepository;
import com.Kusibab.forum.forum.model.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.sql.Timestamp;

@Service
public class CommentService {

    @Autowired
    CommentRepository commentRepository;

    @Autowired
    UserRepository userRepository;

    public void addComment(Comment comment, Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        User user = userRepository.findByUsername(username);
        comment.setUser(user);
        Topic topic = comment.getTopic();
        comment.setPoints(0);
        comment.setDate(new Timestamp(System.currentTimeMillis()));
        user.getCommentList().add(comment);
        topic.getCommentList().add(comment);
        commentRepository.save(comment);
        model.addAttribute("actualComments", topic.getCommentList());
        model.addAttribute("actualTopic", topic);
    }

    public void editComment(Comment comment, Model model) {
        Comment editedComment = commentRepository.findById(comment.getId());
        editedComment.setCommentText("Eddited: (" + new Timestamp(System.currentTimeMillis()) + ") " + comment.getCommentText());
        commentRepository.save(editedComment);
        Topic topic = comment.getTopic();
        model.addAttribute("actualComments", topic.getCommentList());
        model.addAttribute("actualTopic", topic);
    }


}
