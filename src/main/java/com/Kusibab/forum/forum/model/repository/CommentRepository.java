package com.Kusibab.forum.forum.model.repository;

import com.Kusibab.forum.forum.model.entity.Comment;
import com.Kusibab.forum.forum.model.entity.Topic;
import com.Kusibab.forum.forum.model.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends CrudRepository<Comment, Long> {
    Comment findById(Long id);

    List<Comment> findAllByUser(User user);

    List<Comment> findAllByTopic(Topic topic);
}
