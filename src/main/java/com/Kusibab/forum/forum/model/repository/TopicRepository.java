package com.Kusibab.forum.forum.model.repository;

import com.Kusibab.forum.forum.model.entity.Topic;
import com.Kusibab.forum.forum.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TopicRepository extends JpaRepository<Topic, Long> {
    Topic findById(Long id);

    List<Topic> findAllByUser(User user);
}
