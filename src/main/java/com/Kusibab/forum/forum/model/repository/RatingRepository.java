package com.Kusibab.forum.forum.model.repository;

import com.Kusibab.forum.forum.model.entity.Comment;
import com.Kusibab.forum.forum.model.entity.Rating;
import com.Kusibab.forum.forum.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RatingRepository extends JpaRepository<Rating, Long> {

    Rating findByComment(Comment comment);

    Rating findByUser(User user);

    Rating findByUserAndComment(User user, Comment comment);
}
