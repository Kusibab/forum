package com.Kusibab.forum.forum;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ForumApplicationTests {


    @Test
    public void contextLoads() {
    }

    @Test
    public void timeTest() {
        System.out.println("\n\n");
        System.out.println(new Timestamp(System.currentTimeMillis() + 24 * 60 * 60 * 1000));
        System.out.println("\n\n");

    }

}
